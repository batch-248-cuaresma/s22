console.log("JavaScript - Array Manipulation");

//Array Methods
//JS has built-in functions and methods for arrays
//This allows us to manipulate and access array items

//Mutator Methods
/*
	Mutator Methods are functions that "mutate" or change an array after they are created
	These methods manipulate the original array performing various tasks such as adding and removing elements

*/

let fruits = ['Apple',"Orange", 'Kiwi', 'Dragon Fruit'];

//push
	/*
		adds an element in the end of an array and returns the array length

		Syntax:

		arrayName.push();

	*/
	
	console.log("Current Array: ");
	console.log(fruits);

	let fruitsLength = fruits.push("Mango");
	console.log(fruitsLength);
	console.log("Mutated array from push method: ");
	console.log(fruits);//Mango is added at the end of the array

	fruits.push('Avocado', 'Guava');
	console.log("Mutated array from push method: ");
	console.log(fruits);// Avocado and Guava is added at the end of the array
	//['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado', 'Guava']

//pop

/*
	Removes the last element in an array and returns the removed element
	Syntax:
	
	arrayName.pop();

*/

	let removedFruit = fruits.pop();
	console.log(removedFruit);//Guava
	console.log("Mutated array from pop method: ");
	console.log(fruits);// Guava is removed
	//['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']




	let ghostFighters = ["Eugene","Dennis","Alfred","Taguro"];

	/*
		Mini Activity 1 (3min)
		Create a function which will remove the last person in the array
		log the ghostFighters array in the console
		Send an ss of your output

	*/

	function removePerson(){

		ghostFighters.pop();
	}

	removePerson();
	console.log(ghostFighters);//['Eugene', 'Dennis', 'Alfred']

	//unshift()

	/*
		adds one or more elements at the beginning of an array

		syntax
			arrayName.unshift(elementA);
			arrayName.unshift(elementA, elementB);
	*/

		fruits.unshift('Lime', 'Banana');
		console.log("Mutated array from unshift method: ");
		console.log(fruits);//['Lime', 'Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']


	//shift()

	/*
		removes an element at the beginning of an array AND returns the removed element

		syntax
			arrayName.shift();

	*/

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log("Mutated array from shift method: ");
	console.log(fruits);//['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

	//splice
	/*
		simultaneously removes elements from a specified index number and adds the elements

		Syntax
		arrayName.splice(startingIndex,deleteCount,elementsToBeAdded);

	*/

	fruits.splice(1,2,'Lime','Cherry');
	console.log("Mutated array from splice method: ");
	console.log(fruits);//['Banana', 'Lime', 'Cherry', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

	//sort
	 /*
		Rearranges the array elements in alphanumeric order
		-Syntax
			arrayName.sort();

	 */

	fruits.sort();
	console.log("Mutated array from sort method");
	console.log(fruits);

	//reverse
	 /*
		Reverses the order of array elements
		-Syntax
			arrayName.reverse();
	 */

	fruits.reverse();
	console.log("Mutated array from reverse method");
	console.log(fruits);


	//Non-Mutator Methods
	/*
		Non-Mutator methods are functions that do not modify or change an array after they are created
		-these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
	*/

let countries = ['US', "PH", "CAN", "SG",
"TH", "PH","FR", "DE"]

//indexOf()


let firstIndex = countries.indexOf("ph");
console.log("Result of indexOF method: " + firstIndex);
let firstIndex1 = countries.indexOf("PH");
console.log("Result of indexOF method: " + firstIndex1);
let firstIndex2 = countries.indexOf("LK");
console.log("Result of indexOF method: " + firstIndex2);

let lastIndex = countries.lastIndexOf("PH", 3)
console.log("Result of lastIndexOF method: " + lastIndex);


//slice()

console.log(countries)

let sliceedArrayA = countries.slice(2);
console.log("Result from slice method: " + sliceedArrayA);
console.log(countries);

let sliceedArrayB = countries.slice(2,4);
console.log("Result from slice method: " + sliceedArrayB);

let sliceedArrayC = countries.slice(-3);
console.log("Result from slice method: " + sliceedArrayC);

//toString

let stringArray = ghostFighters.toString()
console.log("Result from slice method: " + stringArray);
console.log(typeof stringArray)

//concat()
    let taskArrayA =["drink html", "eat javascript"] ;
    let taskArrayB =["inhale css", "breathe react"];
    let taskArrayC =["get git", "be node"] ;

    let tasks = taskArrayA.concat(taskArrayB);

    console.log("Result from concat method: " + tasks)

// combine multiple arrays
console.log("Result from concat method: ")
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC)
console.log(allTasks)

// combine arrays with element

let combineTasks = taskArrayA.concat("smell epress", "throw mongodb")
console.log(combineTasks);

//join

let user = ['jhon', 'jane', "joe", "robert", 'neil']
console.log(user.join());
console.log(user.join(' '));
console.log(user.join(' - '));
console.log(user.join(' * '));


	//Iteration Method

	/*
		Iteration methods are loops designed to perform repetitive tasks on arrays
		Iteration methods loops over all items in an array
		Useful for mani[ulating array data resulting in complex tasks
	*/


    //forEach()
    
    allTasks.forEach(function(task){
        console.log(task)
    })

	/*
		Mini Activity #2
		create a function that can display the ghostFighters one by one in our console
		Invoke the function
		send a screenshot
	*/
    function displayGhostFighters(){
    ghostFighters.forEach(function(e){
        console.log(e);
    })
    }
    displayGhostFighters()


    //forEach with conditional statements
    
    let filteredTasks = []
    allTasks.forEach(function(e){
        console.log(e)
        if(e.length > 10){
            console.log(e + " grater than 10")
            filteredTasks.push(e)
        }
    })

    console.log("Result of filteredTasks: ");
    console.log(filteredTasks);

//map()

let numbers =[1,2,3,4,5]
let numberMap = numbers.map(function(num){
    return num * num;
})
console.log("original array")
console.log(numbers);
console.log("Result");
console.log(numberMap);

//map() vs forEach()
let numberForEach = numbers.forEach(function(num){
    return num * num;
})
console.log(numberForEach);

//every()

let allValid = numbers.every(function(n){
    return (n < 3);
})

console.log(allValid);

//some()

let someValid = numbers.some(function(n){
    return (n < 2);
})

console.log(someValid); 

//filter()

let filterValid = numbers.filter(function(num){
    return (num < 3);
})
console.log(filterValid); 

let nothingsFound = numbers.filter(function(num){
    return (num == 0);
})
console.log(nothingsFound); 

let filterNumbers = [];
numbers.forEach(function(num){
    if(num < 3){
        filterNumbers.push(num);
    }
})
console.log(filterNumbers)

//includes()

let products = ["Mouse","Keyboard", "Laptop", "Monitor"]
let productsFound = products.includes('mouse');
console.log(productsFound);
let productsFound1 = products.includes('Mouse');
console.log(productsFound1);

let productsNotFound = products.includes('Headset');
console.log(productsNotFound);

//Method chaining 

let filteredProducts = products.filter(function(i){
    return i.toLocaleLowerCase().includes('a');
})
console.log(filteredProducts);

	/*
		Mini Activity #3
		Create an addTrainer function that will enable us to add a trainer in the contacts array
		--This function should be able to receive a string
		--Determine if the added trainer already exists in the contacts array:
		--if it is, show an alert saying "Already added in the Match Call(Contacts)"
		--if it is not, add the trainer in the contacts array and show an alert saying "Registered!"
		--invoke and add a trainer in the browser's console
		--in the console, log the contacts array
	*/

    //reduce 
    console.log(numbers);
    let iteration = 0;

    let reducedArray = numbers.reduce(function(x,y){
        console.log("Current iteration " + ++iteration);
        console.log("accumulator " + x);
        console.log("current value " + y);
        return x + y;
    })

    console.log("Result " + reducedArray);

    let iterationStr = 0;

    let list = ["Hello", "Again", "World"]

    let reducedJoin = list.reduce(function(x,y){
        console.log("Current iteration " + ++iterationStr);
        console.log("accumulator " + x);
        console.log("current value " + y);
        return x + " " + y
    })

    console.log(reducedJoin);

    